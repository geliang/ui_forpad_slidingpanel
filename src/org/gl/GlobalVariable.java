package org.gl;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.impl.client.DefaultHttpClient;
import org.gl.utils.AndroidUtil;
import org.gl.utils.DebugUtil;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Environment;
import android.util.Log;
import android.view.WindowManager;


/**
 * 創建�??MyApplication类，管理打开的应用程�?当程序�?出时finish Activity call
 * by：InstantActicity
 * 
 * @author gl
 * 
 */
public class GlobalVariable extends Application {
	/**
	 * 当前直播的设�?
	 */
	public static String currentLiveRoomID = "0";
	private static List<Activity> activityList = new LinkedList<Activity>();
	private DetectSdcard in;
	private static GlobalVariable instance;
	public static Context applicationContext;
	public final static boolean MltiScreenAble = false;
	public static String Server_IP = "192.168.1.21";
	public static String RESDOWNLOADURL = "http://" + Server_IP
			+ "/JsScript/JsSrcCenter/SrcDown.php";
	/**
	 * 屏幕宽（单位为像素）
	 */
	public static int disPlay_w = 0;
	/**
	 * 屏幕高（单位为像素）
	 */
	public static int disPlay_h = 0;
	/**
	 * 没有外置存储卡则�?cacheDir:/data/data/com.zyfkj.rnbs/files/"否则为："/sdcard/
	 * rnbs/"
	 */
	public static String AppFileSystemDir;
	public static DefaultHttpClient DefaultHttpClient;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		instance = this;
		applicationContext = getApplicationContext();
		Server_IP = AndroidUtil.getServerIPConfig();
		RESDOWNLOADURL = "http://" + Server_IP
				+ "/JsScript/JsSrcCenter/SrcDown.php";
		initFileSystem();
		WindowManager wm = ((WindowManager) applicationContext
				.getSystemService(WINDOW_SERVICE));
		Point point = new Point();
		wm.getDefaultDisplay().getSize(point);
		disPlay_h = point.y;
		disPlay_w = point.x;
		DefaultHttpClient = new org.apache.http.impl.client.DefaultHttpClient();
		// 注册文件系统变动观察�?
		registFileSystemObser();

	}

	/**
	 * /注册文件系统变动观察�?
	 */
	private void registFileSystemObser() {
		in = new DetectSdcard();
		IntentFilter intentf = new IntentFilter();
		intentf.addAction(Intent.ACTION_MEDIA_MOUNTED);// 已挂载SDCARD
		intentf.addAction(Intent.ACTION_MEDIA_UNMOUNTED);// 未挂载SDCARD
		intentf.addAction(Intent.ACTION_MEDIA_BAD_REMOVAL);// 恶意拔SDCARD
		intentf.addAction(Intent.ACTION_MEDIA_SHARED);// 被挂起SDCARD
		intentf.addAction(Intent.ACTION_MEDIA_UNMOUNTABLE);// 无法挂载SDCARD
		// 隐式intent�?��加上下面这句作匹配，否则接收不到广播
		intentf.addDataScheme("file");
		registerReceiver(in, intentf);
	}

	/**
	 * 初始化文件系�?
	 */
	public void initFileSystem() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			File file = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath()
					+ File.separator
					+ "rnbs"
					+ File.separator);
			if (!file.exists()) {
				if (!file.mkdirs()) {
					Log.e(this.getClass().getSimpleName(), "外置文件存储目录创建失败");
					AppFileSystemDir = file.getAbsolutePath() + File.separator;
				} else {
					AppFileSystemDir = file.getAbsolutePath() + File.separator;
				}
				;
			} else {
				AppFileSystemDir = file.getAbsolutePath() + File.separator;
			}
		} else {
			AppFileSystemDir = getFilesDir().getAbsolutePath() + File.separator;
		}
		DebugUtil.println("文件系统当前目录:" + AppFileSystemDir);
	}

	/**
	 * 得到�?��当前的安全的文件存储目录,防止可能的外置存储器错误，如 sdcard被移�?
	 */
	public String getSafeCacheFileDir() {
		initFileSystem();
		return AppFileSystemDir;
	}

	// �?��sdcard是否在机器中
	public class DetectSdcard extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)) {
				// 挂载�?..
				getSafeCacheFileDir();
			} else if (intent.getAction().equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
				// 非挂�?..
				getSafeCacheFileDir();
			} else if (intent.getAction().equals(
					Intent.ACTION_MEDIA_BAD_REMOVAL)) {
				getSafeCacheFileDir();
			} else if (intent.getAction().equals(Intent.ACTION_MEDIA_SHARED)) {
				getSafeCacheFileDir();
			} else if (intent.getAction().equals(
					Intent.ACTION_MEDIA_UNMOUNTABLE)) {

				getSafeCacheFileDir();
			}
			DebugUtil.println("app file system path:" + AppFileSystemDir);
		}

	}

	/**
	 * 单例模式中获取唯�?��MyApplication实例
	 * 
	 * @return
	 */
	public static GlobalVariable getInstance() {
		if (null == instance) {
			instance = new GlobalVariable();
		}
		return instance;

	}

	/**
	 * 添加Activity到容器中
	 * 
	 * @param activity
	 */
	public void addActivity(Activity activity) {
		activityList.add(activity);
	}

	/**
	 * 遍历�?��Activity并finish
	 */
	public void exit() {
		if (in != null) {
			try {
				this.unregisterReceiver(in);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		for (Activity activity : activityList) {
			activity.finish();
		}
		System.exit(0);
	}

	public Activity getActivity(String flag) {
		for (Activity activity : activityList) {
			if (flag.equals(activity.getLocalClassName())) {
				return activity;
			}
		}
		return null;
	}

	// public static void sendNetExceprionBoast() {
	// Context mcontext = activityList.get(activityList.size());
	// if(!AndroidUtil.checkNetwork()){
	// AndroidUtil.genDialog(mcontext,"net err:",applicationContext.getResources().getString(R.string.net_error),new
	// DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// Intent intent = new Intent("android.settings.WIRELESS_SETTINGS");
	// applicationContext.startActivity(intent);
	// }
	// });
	// }else{
	// AndroidUtil.genDialog(mcontext,
	// "logout",applicationContext.getResources().getString(R.string.disconnect_server),new
	// DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// getInstance().exit();
	// }
	// });
	// };
	//
	// }
}
