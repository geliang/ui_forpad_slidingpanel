package org.gl.utils;
/**
 * <p>Title:整型与长度为4的字节数组的互换 </p>
 */   
public class ByteUtil {
	/**
	 * 整型转换为4位字节数组
	 * @param intValue
	 * @return
	 */
	public static byte[] int2Byte(int intValue) {
		byte[] b = new byte[4];
		for (int i = 0; i < 4; i++) {
			b[i] = (byte) (intValue >> 8 * (i) & 0xFF);
		}
		return b;
	}
	/**
	 * 4位字节数组转换为整型
	 * @param b
	 * @return
	 */
	public static int byte2Int(byte[] b) {
		int intValue = 0;
		for (int i = 0; i < 4; i++) {
			intValue += (b[i] & 0xFF) << (8 * (i));
		}
		return intValue;
	}

}