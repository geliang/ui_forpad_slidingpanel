package org.gl.utils;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPUtil {

	// /**
	// * @param args
	// */
	// public static void main(String[] args) {
	// // TODO Auto-generated method stub
	//
	// }
	/**
	 * 分割符号 . - _
	 * 
	 * @param spertor
	 * @return
	 */
	public String getMacAddr(String spertor) {
		if (spertor ==null) {
			spertor = "-";
		}
		StringBuilder MacAddr = new StringBuilder();
		// String str = "";
		try {
			Enumeration<NetworkInterface> em = NetworkInterface
					.getNetworkInterfaces();
			while (em.hasMoreElements()) {
				NetworkInterface nic = em.nextElement();
				// System.out.println("NetworkInterface.getDisplayName ():"
				// + nic.getDisplayName());
				// System.out.println("NetworkInterface.getName ():" +
				// nic.getName());
				byte[] b = nic.getHardwareAddress();
				if (b == null) {
					continue;
				}
				for (int i = 0; i < b.length; i++) {
					if (i == 0) {
						MacAddr.append(byteHEX(b[i]));
					} else {
						MacAddr.append(spertor + byteHEX(b[i]));
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
			// System.exit(-1);
		}
		System.out.print("MacAddress:" + MacAddr);
		return MacAddr.toString();
	}

	public String getLocalIP() {
		String ip = "";
		try {
			Enumeration<?> e1 = (Enumeration<?>) NetworkInterface
					.getNetworkInterfaces();
			while (e1.hasMoreElements()) {
				NetworkInterface ni = (NetworkInterface) e1.nextElement();
				Enumeration<?> e2 = ni.getInetAddresses();
				while (e2.hasMoreElements()) {
					InetAddress ia = (InetAddress) e2.nextElement();
					if (!ia.isLoopbackAddress()) {
						ip = ia.getHostAddress();
						System.out.println("ip:" + ip);
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return ip;
	}

	/* 一个将字节转化为十六进制ASSIC码的函数 */
	public static String byteHEX(byte ib) {
		char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
				'b', 'c', 'd', 'e', 'f' };
		char[] ob = new char[2];
		ob[0] = Digit[(ib >>> 4) & 0X0F];
		ob[1] = Digit[ib & 0X0F];
		String s = new String(ob);
		return s;
	}

	public static String PublicWebServerIP = "http://www.whereismyip.com";

	private static String getFirstIp(StringBuffer wanPacket, String pattern) {
		Pattern p;
		if (pattern == null) {
			p = Pattern.compile("\\d+\\.\\d+\\.\\d+\\.\\d+");
		} else {
			p = Pattern.compile(pattern);
		}
		// 找出数据包中第一个匹配的IP,即为Ip

		Matcher m = p.matcher(wanPacket);
		if (m.find()) {
			return m.group();
		} else {
			return null;
		}
	}

	public String getWebIp(String strUrl) {
		if (strUrl == null) {
			strUrl = PublicWebServerIP;
		}
		try {

			URL url = new URL(strUrl);

			BufferedReader br = new BufferedReader(new InputStreamReader(url

			.openStream()));

			String s = "";

			StringBuffer sb = new StringBuffer("");

			// String webContent = "";

			while ((s = br.readLine()) != null) {
				sb.append(s + "\r\n");

			}

			br.close();
			// webContent = sb.toString();
			// System.out.println(sb.toString());
			// int start = webContent.indexOf("[") + 1;
			// int end = webContent.indexOf("]");
			// webContent = webContent.substring(start, end);
			// String ip = getFirstIp(sb, null);
			return getFirstIp(sb, null);
		} catch (Exception e) {
			e.printStackTrace();
			return "error open url:" + strUrl;

		}
	}

}
