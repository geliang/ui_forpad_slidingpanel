package org.gl.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.gl.GlobalVariable;

/**
 * 鏃ュ織鎺у埗绫�
 * @author zhaoshuming
 * @date 2012-12-28
 */
public class DebugUtil {
	private static boolean isDebug = true;
	private static String defaultFileDir = GlobalVariable.AppFileSystemDir;
	private static String defaultFileName = "log.txt";
	/**
	 * syso 鎵撳嵃
	 * @param string
	 */
	public static void println(Object string){
		if (isDebug) {
			System.out.println(string);
		}
	}
	/**
	 * 杈撳嚭鍒版枃浠�
	 * @param string 缁濆璺緞
	 */
	public static void outToFile(Object string,String filePath){
		if (isDebug) {
			if ((null!=filePath)&&(!"".equals(filePath))&&filePath.length()>0) {
			}else{
				filePath = defaultFileDir +defaultFileName;
			}
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(new File(filePath));
				fos.write(string.toString().getBytes());
				fos.flush();
				fos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally{
				if (fos!=null) {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					fos = null;
				}
			}
			
		}
	}
}
