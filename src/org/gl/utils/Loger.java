package org.gl.utils;

import android.util.Log;
/**
 * logger switch 
 * @author zyfgeliang
 *
 */
public class Loger {
	private final static int logDegree =Integer.MIN_VALUE;
	private final static int logDegree_base =-1;
	/**
	 * view层用的日志标签 变量名前加z
	 */
	public final static int z_logDegree_View_mainActivity=logDegree_base+1;
	
	public final static int z_logDegree_View_mainActivity_OnGestureListener=logDegree_base+2;
	/**
	 * DATA层用的日志标签
	 */
	public final static int z_logDegree_Data =logDegree_base+3;
	public final static int z_logDegree_Data_ResourcesInfo =logDegree_base+4;
	public final static int z_logDegree_Data_ResourcesInfo_download =logDegree_base+5;
	
	/**
	 * 点播界面的日志
	 */
	public final static int z_logDegree_View_vodActivity=logDegree_base+5;
	public final static int z_logDegree_View_opengl=logDegree_base+6;
	public static void info(int tag, String info ){
		if (tag>logDegree) {
			Log.i(String.valueOf(tag), info);
		}
	};
	public static void err(int tag, String info ){
		if (tag>logDegree) {
			Log.e(String.valueOf(tag), info);
		}
	};
	
}
