package org.gl.ui.manger;

import org.gl.R;
import org.gl.ui.manger.BaseActivityGroup.ItemClickEvent;
import org.gl.utils.AndroidUtil;

import com.slidingpad.MainActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

@SuppressWarnings("deprecation")
public class LeftNavActivityGroup extends BaseActivityGroup implements
		BaseActivityGroup.InitActivityGroup {
	private Integer[] tabImages = { android.R.drawable.ic_delete,
			android.R.drawable.btn_dialog,
			android.R.drawable.btn_dropdown // tab标签图标
	};
	private Integer[] tabImages_sel = { android.R.drawable.menu_full_frame,
			android.R.drawable.menu_full_frame,
			android.R.drawable.menu_full_frame // tab标签图标
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// gv_tabPage.setNumColumns(tabImages.length);// 设置列数
		// gv_tabPage.setSelector(new ColorDrawable(Color.TRANSPARENT));//
		// 选中的时候为透明色
		// gv_tabPage.setDrawSelectorOnTop(true);
		// gv_tabPage.setGravity(Gravity.CENTER);// 位置居中
		// gv_tabPage.setVerticalSpacing(0);// 垂直间隔
		// imageAdapter = new NavBarAdapter(this, tabImages, tabImages_sel,
		// AndroidUtil.dipToPx(140), AndroidUtil.dipToPx(62)); //
		// 创建图片适配器，传递图片所需高和宽
		// // imageAdapter = new ImageAdapter(this, tabImages, tabImages_sel,
		// 76,
		// // 62); // 创建图片适配器，传递图片所需高和宽
		// gv_tabPage.setAdapter(imageAdapter);// 设置菜单Adapter
		// gv_tabPage.setOnItemClickListener(new ItemClickEvent()); // 注册点击事件
		// SwitchPage(0);// 默认打开第0页
	}

	@Override
	InitActivityGroup Init() {
		return this;
	}

	@Override
	public View getContentView() {
		return mLayoutInflater.inflate(R.layout.rnbs_main, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public AdapterView<NavBarAdapter> initNavBar() {
		GridView gv_tabPage = (GridView) findViewById(R.id.gv_tabPage);
		gv_tabPage.setNumColumns(1);// 设置列数
		gv_tabPage.setSelector(new ColorDrawable(Color.TRANSPARENT));//
		gv_tabPage.setDrawSelectorOnTop(true);
		gv_tabPage.setGravity(Gravity.CENTER);// 位置居中
		gv_tabPage.setVerticalSpacing(0);// 垂直间隔
		return (AdapterView)gv_tabPage;
	}

	@Override
	public NavBarAdapter initNavBarAdapter() {
		return new NavBarAdapter(this, tabImages, tabImages_sel,
				AndroidUtil.dipToPx(160), AndroidUtil.dipToPx(100));
	}

	@Override
	public Intent[] initSubActivityIntet() {
		Intent[] mIntent = new Intent[3];
		for (int i = 0; i < tabImages_sel.length; i++) {
			mIntent[i] = new Intent();
			mIntent[i].setClass(this, BaseActivity.class);
		}
		mIntent[1] = new Intent();
		mIntent[1].setClass(this, MainActivity.class);
		return mIntent;
	}

	@Override
	public LinearLayout initpageContainer() {
		return (LinearLayout) findViewById(R.id.pageContainer);
	}

}
