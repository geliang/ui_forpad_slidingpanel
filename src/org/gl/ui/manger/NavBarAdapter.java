package org.gl.ui.manger;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * 加载楼层适配器，暂时无用
 * 
 * @author zhaoshuming
 * @date 2012-6-14
 */
public class NavBarAdapter extends BaseAdapter {
	private Context context;
	private int width;
	private int height;
	private View[] imageViews;

	private Integer[] tabImages;
	private Integer[] tabImages_sel;

	public NavBarAdapter(Context context, Integer[] tabImages,
			Integer[] tabImages_sel, int width, int height) {
		this.context = context;
		this.tabImages = tabImages;
		this.tabImages_sel = tabImages_sel;
		this.width = width;
		this.height = height;
		imageViews = new View[tabImages.length];
		for (int position = 0; position < imageViews.length; position++) {
			ImageView convertView = new ImageView(context);
			convertView
					.setLayoutParams(new GridView.LayoutParams(width, height));
			convertView.setBackgroundResource(tabImages[position]);
			imageViews[position] = convertView;
		}
	}

	public int getCount() {
		return tabImages.length;
	}

	public Object getItem(int position) {
		return imageViews[position];
	}

	public long getItemId(int position) {
		return position;
	}

	/**
	 * 点击设置
	 * 
	 * @param selectedID
	 */
	public void setFocus(int selectedID) {
		for (int i = 0; i < imageViews.length; i++) {
			imageViews[i].setBackgroundResource(tabImages[i]);
		}
		imageViews[selectedID].setBackgroundResource(tabImages_sel[selectedID]);
		imageViews[selectedID].postInvalidate();

	}

	/**
	 * 图片设置
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		// imageViews[position].setImageResource(tabImages[position]);
		// imageViews[position].setLayoutParams(new GridView.LayoutParams(width,
		// height));
		// if (convertView==null) {
		// convertView
		// =((LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.menuitem,
		// null);
		// convertView.setLayoutParams(new
		// GridView.LayoutParams(GridView.LayoutParams.WRAP_CONTENT, height));
		// }
		//
		// ImageView iv = (ImageView)
		// convertView.findViewById(R.id.menuitem_iv);
		// iv.setBackgroundResource(tabImages[position]);
		// imageViews[position] = iv;
		// System.out.println("fsadfa"+iv.hashCode());
		// TextView tv = (TextView) convertView.findViewById(R.id.menuitem_tv);
		// switch (position) {
		// case 0:
		// tv.setText(context.getResources().getString(R.string.control));
		// break;
		// case 1:
		// tv.setText(context.getResources().getString(R.string.control_server));
		// break;
		//
		// default:
		// tv.setText("no content");
		// break;
		// }
		return imageViews[position];
		// return convertView;
	}

}
