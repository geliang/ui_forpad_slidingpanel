package org.gl.ui.manger;

import org.gl.GlobalVariable;
import org.gl.R;
import org.gl.customview.Transition3d;
import org.gl.utils.AndroidUtil;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
/**
 * a UI frame base on ActivityGroup
 * @author zyfgeliang
 *
 */
@SuppressWarnings("deprecation")
public abstract class BaseActivityGroup extends ActivityGroup {
	public interface InitActivityGroup {
		/**
		 * UI for frame ,this view  should contain a navBarListView and Container of subActivity
		 * @return
		 */
		public View getContentView();
		/**
		 * a adapter of navBarListView extends  NavBarAdapter
		 * @return
		 */
		public AdapterView<NavBarAdapter> initNavBar();
		/**
		 * a ContainerView to show the view of subActivity
		 * @return
		 */
		public LinearLayout initpageContainer();
		/**
		 * a adapter of navBarListView extends  NavBarAdapter
		 * @return
		 */
		public NavBarAdapter initNavBarAdapter();
		/**
		 * the intent to subActivity
		 * @return
		 */
		public Intent[] initSubActivityIntet();
	}

	abstract InitActivityGroup Init();

	private int currentView = -1;
	public LinearLayout pageContainer = null;
	private Intent[] intents = new Intent[3];; // 页面跳转Intent
	private View[] subPageView = new View[3]; // 子页面视图View

	public NavBarAdapter imageAdapter;
	private Transition3d t3d;
	InitActivityGroup mInitActivityGroup;
	public LayoutInflater mLayoutInflater;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		GlobalVariable.getInstance().addActivity(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mLayoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		mInitActivityGroup = Init();
		setContentView(mInitActivityGroup.getContentView());
		intents = mInitActivityGroup.initSubActivityIntet();
		
		AdapterView<NavBarAdapter> gv_tabPage = mInitActivityGroup.initNavBar();
		imageAdapter = mInitActivityGroup.initNavBarAdapter();
		gv_tabPage.setAdapter(imageAdapter);
		gv_tabPage.setOnItemClickListener(new ItemClickEvent());
		
		pageContainer = mInitActivityGroup.initpageContainer();
		SwitchPage(0);
	}

	/**
	 * 底部按钮点击事件
	 */
	class ItemClickEvent implements OnItemClickListener {

		public void onItemClick(final AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			if (arg2 != currentView) {
				SwitchPage(arg2); // arg2表示选中的Tab标签号，从0~4
				currentView = arg2;
				arg0.setClickable(false);
				arg0.postDelayed(new Runnable() {

					@Override
					public void run() {
						arg0.setClickable(true);
					}
				}, 2000);
			}

		}
	}

	/**
	 * 根据ID打开指定的PageActivity
	 * 
	 * @param id
	 *            选中项的tab序号
	 */
	public void SwitchPage(int id) {
		imageAdapter.setFocus(id);
		View pageView = null;
		pageView = getPageView(id);
		if (currentView >= 0 && subPageView[currentView] != null
				&& currentView != id && subPageView[id] != null) {
			// 装载子页面View到LinearLayout容器里面
			if (t3d == null) {
				t3d = new Transition3d();
			}
			t3d.applyRotation(subPageView[currentView], pageView, 0, 0, 90);
		}
		pageContainer.removeAllViews();// 必须先清除容器中所有的View
		pageContainer.addView(pageView, LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT);
	}

	/**
	 * 用于获取intent和pageView， 类似于单例模式，使得对象不用重复创建，同时，保留上一个对象的状态
	 * 当重新访问时，仍保留原来数据状态，如文本框里面的值。
	 * 
	 * @param pageID
	 *            选中的tab序号（0~4）
	 * @return
	 */
	private View getPageView(int pageID) {
		if (pageID > intents.length - 1) {
			pageID = intents.length - 1;
		}
		if (pageID < 0) {
			pageID = 0;
		}
		if (subPageView[pageID] == null) {
			subPageView[pageID] = getLocalActivityManager().startActivity(
					"subPageView" + pageID, intents[pageID]).getDecorView();
		}

		return subPageView[pageID];

	}

	@Override
	public void onBackPressed() {

		new AlertDialog.Builder(this)
				.setMessage(getResources().getString(R.string.tips_logout))
				.setPositiveButton(
						getResources().getString(R.string.dialog_yes),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								GlobalVariable.getInstance().exit();
							}
						})
				.setNegativeButton(
						getResources().getString(R.string.dialog_no),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						})
				.setTitle(getResources().getString(R.string.system_exit))
				.show();

	}

}
