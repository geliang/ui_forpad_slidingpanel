package org.gl.customview;


import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;

/**
 * This sample application shows how to use layout animation and various
 * transformations on views. The result is a 3D transition between a
 * ListView and an ImageView. When the user clicks the list, it flips to
 * show the picture. When the user clicks the picture, it flips to show the
 * list. The animation is made of two smaller animations: the first half
 * rotates the list by 90 degrees on the Y axis and the second half rotates
 * the picture by 90 degrees on the Y axis. When the first half finishes, the
 * list is made invisible and the picture is set visible.
 */
public class Transition3d  
       {
    private View previous;
    private View  next;
	private Rotate3dAnimation rotation;

//    public Transition3d (View previous,View next){
//    	previous.clearAnimation();
//    	next.clearAnimation();
//    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.animations_main_screen);
//
//        mPhotosList = (ListView) findViewById(android.R.id.list);
//        mImageView = (ImageView) findViewById(R.id.picture);
//        mContainer = (ViewGroup) findViewById(R.id.container);
//
//        // Prepare the ListView
//        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_list_item_1, PHOTOS_NAMES);
//
//        mPhotosList.setAdapter(adapter);
//        mPhotosList.setOnItemClickListener(this);
//
//        // Prepare the ImageView
//        mImageView.setClickable(true);
//        mImageView.setFocusable(true);
//        mImageView.setOnClickListener(this);
//
//        // Since we are caching large views, we want to keep their cache
//        // between each animation
//        mContainer.setPersistentDrawingCache(ViewGroup.PERSISTENT_ANIMATION_CACHE);
//    }

    /**
     * Setup a new 3D rotation on the container view.<b>view should != other view</b>
     *
     * @param position the item that was clicked to show a picture, or -1 to show the list
     * @param start the start angle at which the rotation must begin
     * @param end the end angle of the rotation
     */
    public void applyRotation(View previous,View next,int position, float start, float end) {
    	System.out.println("applyRotation:"+(previous == next));
    	if (previous==null||next==null||previous == next) {
			return;
		}
    	this.previous = previous;
    	this.next = next;
//    	this.previous.clearAnimation();
//    	this.next.clearAnimation();
    	next.setVisibility(View.GONE);
        // Find the center of the container
        final float centerX = previous.getWidth() / 2.0f;
        final float centerY = previous.getHeight() / 2.0f;

        // Create a new 3D rotation with the supplied parameter
        // The animation listener is used to trigger the next animation
        if (rotation!=null) {
        	rotation = null;
		}
        rotation =
                new Rotate3dAnimation(start, end, centerX, centerY, 310.0f, true);
        rotation.setDuration(500);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new DisplayNextView(position));

        previous.startAnimation(rotation);
        rotation=null;
    }


    /**
     * This class listens for the end of the first half of the animation.
     * It then posts a new action that effectively swaps the views when the container
     * is rotated 90 degrees and thus invisible.
     */
    private final class DisplayNextView implements Animation.AnimationListener {
        private final int mPosition;

        private DisplayNextView(int position) {
            mPosition = position;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
        	previous.post(new SwapViews(mPosition));
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    /**
     * This class is responsible for swapping the views and start the second
     * half of the animation.
     */
    private final class SwapViews implements Runnable {
        private final int mPosition;

        public SwapViews(int position) {
            mPosition = position;
        }

        public void run() {
            final float centerX = previous.getWidth() / 2.0f;
            final float centerY = previous.getHeight() / 2.0f;

            if (mPosition > -1) {
            	next.setVisibility(View.VISIBLE);
            	previous.setVisibility(View.GONE);
//                rotation = new Rotate3dAnimation(90, 180, centerX, centerY, 310.0f, false);
                rotation = new Rotate3dAnimation(-90, 0, centerX, centerY, -310.0f, false);
                rotation.setDuration(500);
                rotation.setFillAfter(true);
                rotation.setInterpolator(new DecelerateInterpolator());
                rotation.setAnimationListener(new AnimationListener() {
					
					public void onAnimationStart(Animation animation) {
						// 
						
					}
					
					public void onAnimationRepeat(Animation animation) {
						// 
						
					}
					
					public void onAnimationEnd(Animation animation) {
						previous.setVisibility(View.VISIBLE);
						next.requestFocus();
					}
				});
                next.startAnimation(rotation);
            } else {

            
                rotation = new Rotate3dAnimation(90, 0, centerX, centerY, 310.0f, false);
                rotation.setDuration(500);
                rotation.setFillAfter(true);
                rotation.setInterpolator(new DecelerateInterpolator());
                next.startAnimation(rotation);
//            	next.setVisibility(View.GONE);
//            	previous.setVisibility(View.VISIBLE);
            	previous.requestFocus();

            }
            rotation = null;

        }
    }

}
