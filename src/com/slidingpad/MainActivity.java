package com.slidingpad;

import org.gl.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;


public class MainActivity extends Activity {
	public static boolean c = true;
	public static boolean d = false;
	public static int _orientation;

	public MainActivity() {
	}

	public void onCreate(Bundle paramBundle) {
		getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
		super.onCreate(paramBundle);
		_orientation = getResources().getConfiguration().orientation;

		setContentView(R.layout.main_landscape);

		SlidingPanelGroup _slidingStage = (SlidingPanelGroup) findViewById(R.id.slidingStage);
		_slidingStage.welcome();
		Log.d("onCreate", "");
		// this._mainLayout.loadFirstPanel();
	}

}
