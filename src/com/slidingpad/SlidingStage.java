package com.slidingpad;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public abstract class SlidingStage extends FrameLayout {
	protected boolean a = false;
	protected boolean b = false;
	protected FrameLayout _slidingA;
	protected FrameLayout _slidingB;

	public SlidingStage(Context context) {
		this(context, null);
	}

	public SlidingStage(Context context, AttributeSet paramAttributeSet) {
		super(context, paramAttributeSet);
		this._slidingA = new FrameLayout(context);
		this._slidingA.setVisibility(View.INVISIBLE);
		this._slidingB = new FrameLayout(context);
		this._slidingB.setVisibility(View.INVISIBLE);
	}

	protected final void g() {
		View view = findFocus();
		if (view != null) {
			view.onWindowFocusChanged(true);
			view.clearFocus();
		}

	}

	public abstract void welcome();

	public abstract void slideIn();

}
