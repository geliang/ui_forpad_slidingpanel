package com.slidingpad;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 双面板滑动控制组件<br>
 * 未660+360px 以上宽度的宽屏设备设计
 * 
 * @author zyfgeliang
 * 
 */
public class SlidingPanelGroup extends SlidingStage {

	private int _slideWidthA;
	private int _marginLeftA;
	/**
	 * 只显示面板A距离左边的宽度
	 */
	private final static int MARGINLEFT_PANEL_A = 60;
	private final static int MARGINLEFT_PANEL_A_B = 48;
	/**
	 * 面板A宽度
	 */
	private final int SlideWidthA = 360;//
	/**
	 * 面板B的宽度
	 */
	private final int SlideWidthB = 660;//
	private int _slideWidthB;
	private int _marginLeftB;
	/**
	 * 最后点击的点X坐标
	 */
	private float mLastMotionX;//
	/**
	 * 最后点击的点Y坐标
	 */
	private float mLastMotionY;//
	/**
	 * 面板当前所在X轴拖动坐标
	 */
	private int _move = 0;

	private final static int TOUCH_STATE_REST = 0;
	private final static int TOUCH_STATE_SCROLLING = 1;
	private int mTouchSlop;
	private int mTouchState = TOUCH_STATE_REST;

	private Scroller _scroller;
	private Animation _animDisappear;

	private Context _context;

	public SlidingPanelGroup(Context context, AttributeSet paramAttributeSet) {
		super(context, paramAttributeSet);
		this._context = context;
		this._slideWidthA = SlideWidthA;
		this._marginLeftA = MARGINLEFT_PANEL_A;
		this._slideWidthB = SlideWidthB;
		// this._marginLeftB = MARGINLEFT_PANEL_B;

		this._animDisappear = getDisappearAnim();
		final ViewConfiguration configuration = ViewConfiguration.get(context);
		// 获得可以认为是滚动的距离
		mTouchSlop = configuration.getScaledPagingTouchSlop() * 3;
		this._scroller = new Scroller(context, new OvershootInterpolator(1.5F));

		TextView tv = new TextView(context);
		tv.setText(Html.fromHtml("<br/><br/><br/> The Panel A<br/><br/>"));
		tv.setTextSize(30F);
		tv.setTextColor(android.graphics.Color.BLUE);
		this._slidingA.addView(tv, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);

		Button btn = new Button(context);
		btn.setText("Show Panel B");
		OnClickListener mButtonOnclick = new OnClickListener() {

			@Override
			public void onClick(View v) {
				showSlideAB();
				Toast.makeText(_context, "onclick", Toast.LENGTH_SHORT).show();
			}
		};
		btn.setOnClickListener(mButtonOnclick);
		this._slidingA.addView(btn, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		ListView listview = new ListView(context);
		BaseAdapter baseadpater = new BaseAdapter() {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				Button btn = new Button(_context);
				btn.setText("Show Panel B");
				btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						showSlideAB();
					}
				});
				return btn;
			}

			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return position;
			}

			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return 100;
			}
		};
		listview.setAdapter(baseadpater);
		this._slidingA.addView(listview, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		FrameLayout.LayoutParams layoutParamA = new FrameLayout.LayoutParams(
				this._slideWidthA, ViewGroup.LayoutParams.FILL_PARENT);
		// layoutParamA.setMargins(_marginLeftA, 0, 0, 0);
		this._slidingA.setLayoutParams(layoutParamA);
		// this._slidingA.offsetLeftAndRight(_marginLeftA);
		addView(this._slidingA);// 添加面板A

		FrameLayout.LayoutParams layoutParamB = new FrameLayout.LayoutParams(
				this._slideWidthB, ViewGroup.LayoutParams.FILL_PARENT);
		Button btn2 = new Button(context);
		btn2.setText("Click Me  ");
		btn2.setOnClickListener(mButtonOnclick);
		TextView tvB = new TextView(context);
		tvB.setText(Html.fromHtml("<br/><br/><br/><br/> The Panel B<br/><br/>"));
		tvB.setTextSize(30F);
		tvB.setTextColor(android.graphics.Color.BLUE);
		LinearLayout ll2 = new LinearLayout(_context);
		ll2.setOrientation(LinearLayout.VERTICAL);
		this._slidingB.addView(ll2, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		ll2.addView(btn2, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		ll2.addView(tvB, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		GridView gv_tabPage = new GridView(_context);
		gv_tabPage.setNumColumns(5);// 设置列数
		gv_tabPage.setSelector(new ColorDrawable(Color.TRANSPARENT));//
		gv_tabPage.setDrawSelectorOnTop(true);
		gv_tabPage.setGravity(Gravity.CENTER);// 位置居中
		gv_tabPage.setVerticalSpacing(20);// 垂直间隔
		gv_tabPage.setHorizontalSpacing(20);// 垂直间隔
		gv_tabPage.setAdapter(baseadpater);
		ll2.addView(gv_tabPage, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		ll2.setLayoutParams(layoutParamB);
		addView(this._slidingB, this._slideWidthB, LayoutParams.FILL_PARENT);

	}

	public abstract interface onAdjustFinishListener {
		public abstract void a();
	}

	/**
	 * 面板A 进入，如果面板可见直接返回
	 */
	@Override
	public void slideIn() {
		if (this._slidingA.getVisibility() == View.VISIBLE
				&& this._slidingB.getVisibility() == View.VISIBLE) {
			showSlideAB();
		} else if (this._slidingA.getVisibility() == View.VISIBLE
				&& this._slidingB.getVisibility() != View.VISIBLE) {
			showSlideAB();
		} else if (this._slidingA.getVisibility() != View.VISIBLE
				&& this._slidingB.getVisibility() != View.VISIBLE) {
			showSlideA();
		}
	}

	public void showSlideA() {
		_marginLeftA = MARGINLEFT_PANEL_A;
		AnimationSet animSet = new AnimationSet(true);
		animSet.setInterpolator(new OvershootInterpolator(1.5F));
		TranslateAnimation transAnim = new TranslateAnimation(
				this._slideWidthA, 0.0F, 0.0F, 0.0F);
		AlphaAnimation alphaAnim = new AlphaAnimation(0.0F, 1.0F);
		animSet.addAnimation(alphaAnim);
		animSet.addAnimation(transAnim);
		animSet.setDuration(400L);
		this._slidingA.startAnimation(animSet);
		this._slidingA.setVisibility(View.VISIBLE);
		requestLayout();
	}

	/**
	 * 双面板 滑出
	 * 
	 * @param _move2
	 */
	public void slideOut() {
		Log.d("slideOut",
				"_marginLeftA:" + _marginLeftA + ";"
						+ this._slidingA.getVisibility() + " "
						+ this._slidingB.getVisibility());

		// if (this._slidingA.getVisibility() == View.VISIBLE
		// && this._slidingB.getVisibility() == View.VISIBLE) {// A 显示是全部
		// B面板显示全部的情况
		// shrinkSlideB();
		// } else
		if (this._slidingA.getVisibility() == View.VISIBLE
				&& this._slidingB.getVisibility() == View.VISIBLE) {// //A 显示是全部
																	// B面板显示一半的情况
			_marginLeftA = MARGINLEFT_PANEL_A;
			this._slidingB.startAnimation(this._animDisappear);
			this._slidingB.setVisibility(View.INVISIBLE);
			requestLayout();
		} else if (this._slidingA.getVisibility() == View.VISIBLE
				&& (this._slidingB.getVisibility() == View.INVISIBLE || this._slidingB
						.getVisibility() == View.GONE)) {// A 显示是全部 B面板没有显示
			this._slidingA.setVisibility(View.INVISIBLE);
			this._slidingA.startAnimation(this._animDisappear);
		}else{
			return;
		}
	}

	/**
	 * 面板B显示一半 面板A显示全部
	 */
	protected void shrinkSlideB() {
		if (this._slidingB.getVisibility() != View.VISIBLE) {
			return;
		}
		if (!this._scroller.isFinished()) {
			this._scroller.abortAnimation();
			this._scroller.forceFinished(true);
		}
		TranslateAnimation transAnim1 = new TranslateAnimation(-244.0F, 0.0F,
				0.0F, 0.0F);
		TranslateAnimation transAnim2 = new TranslateAnimation(
				this._slideWidthB - 950, 0.0F, 0.0F, 0.0F);
		transAnim1.setDuration(270L);
		transAnim2.setDuration(270L);

		Log.d("shrinkSlideB", "_marginLeftB:" + _marginLeftB);
		this._slidingA.startAnimation(transAnim1);
		this._slidingB.startAnimation(transAnim2);
		_marginLeftA = 306;
		_marginLeftB = 950;
		requestLayout();
	}

	/**
	 * 面板AB全部显示
	 */
	protected void showSlideAB() {
		if (this._slidingB.getVisibility() == View.VISIBLE) {
			return;
		}
		TranslateAnimation transAnimA = new TranslateAnimation(244.0F, 0.0F,
				0.0F, 0.0F);
		TranslateAnimation transAnimB = new TranslateAnimation(
				this._slideWidthB, 0.0F, 0.0F, 0.0F);
		transAnimA.setDuration(270L);
		transAnimA.setStartOffset(230L);
		transAnimB.setDuration(500L);
		this._slidingA.startAnimation(transAnimA);
		this._slidingB.startAnimation(transAnimB);
		this._slidingB.setVisibility(View.VISIBLE);
		_marginLeftA = MARGINLEFT_PANEL_A_B;
		_marginLeftB = MARGINLEFT_PANEL_A_B+_slideWidthA-60;
		requestLayout();
	}

	@Override
	public void welcome() {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if (_slidingA.getVisibility() == View.GONE
						|| _slidingA.getVisibility() == View.INVISIBLE) {
					slideIn();
				}
			}
		};
		this._slidingA.postDelayed(runnable, 800L);
	}

	protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2,
			int paramInt3, int paramInt4) {
		Log.d("onlayout", "_marginLeftA:" + _marginLeftA);
		super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
		this._slidingA.layout(_marginLeftA, this._slidingA.getTop(),
				(_marginLeftA + _slideWidthA), this._slidingA.getBottom());
		this._slidingB.layout(_marginLeftB, this._slidingB.getTop(),
				(_marginLeftB + _slideWidthB), this._slidingB.getBottom());
	}

	private Animation getDisappearAnim() {
		AnimationSet animSet = new AnimationSet(true);
		animSet.setInterpolator(new LinearInterpolator());
		AlphaAnimation alphaAnim = new AlphaAnimation(1.0F, 0.0F);
		float f = this._slideWidthA / 3.0F;
		TranslateAnimation transAnim = new TranslateAnimation(0.0F, f, 0.0F,
				0.0F);
		animSet.addAnimation(alphaAnim);
		animSet.addAnimation(transAnim);
		animSet.setDuration(200L);
		return animSet;
	}

	@Override
	public void computeScroll() {
		if (_scroller.computeScrollOffset()) {
			// 返回当前滚动X方向的偏移
			scrollTo(_scroller.getCurrX(), 0);
			postInvalidate();
		}
	}

	public boolean onInterceptTouchEvent(MotionEvent ev) {
		final float x = ev.getX();
		final float y = ev.getX();
		Log.d("SlidingStageLandscape.InterceptTouchEvent", "ev.getX():" + x
				+ "; ev.getAction():" + ev.getAction());
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mLastMotionX = x;
			mLastMotionY = x;
			mTouchState = _scroller.isFinished() ? TOUCH_STATE_REST
					: TOUCH_STATE_SCROLLING;
			mTouchState = TOUCH_STATE_REST;

			break;
		case MotionEvent.ACTION_MOVE:
			final int xDiff = (int) Math.abs(x - mLastMotionX);
			final int yDiff = (int) Math.abs(y - mLastMotionY);

			// 判断是否是拖动面板的手势 如果是 则打断触摸事件的向下传递
			if (xDiff > mTouchSlop && yDiff > mTouchSlop
					&& mTouchState != TOUCH_STATE_SCROLLING) {
				mTouchState = TOUCH_STATE_SCROLLING;
				Log.d("SlidingStageLandscape.InterceptTouchEvent",
						"xDiff > mTouchSlop,it`s planel sroll event,return true to InterceptTouchEvent");
				return true;
			} else {
				break;
			}
		case MotionEvent.ACTION_UP:
			mTouchState = TOUCH_STATE_REST;
			break;
		}
		return false;
	}

	private final String tag = "SildingPanelGroup";

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.i(tag, "onTouchEvent");
		onSildEvent(event);
		return true;
	}

	/**
	 * 处理面板的手势拖动事件
	 * 
	 * @param ev
	 */
	private boolean onSildEvent(MotionEvent ev) {// TODO 两个面板都没显示的情况下
													// 通过手势控制面板的显示隐藏
		final float x = ev.getX();
		Log.d("onSildEvent", "getX: " + x);
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (!_scroller.isFinished()) {
				_scroller.forceFinished(true);
				_move = _scroller.getFinalX();
			}
			mLastMotionX = x;
			break;
		case MotionEvent.ACTION_MOVE:
			if (ev.getPointerCount() == 1) {// 单点拖动
				int deltaX = 0;
				deltaX = (int) (x - mLastMotionX);
				Log.d("Moving", "deltaX: " + deltaX + "; move:" + _move);
				scrollTo(-(_move + deltaX), 0);
			}
			break;
		case MotionEvent.ACTION_UP:
			mTouchState = TOUCH_STATE_REST;
			int deltaX = 0;
			deltaX = (int) (x - mLastMotionX);
			// _move = (_move + deltaX);
			int _move_real = (_move + deltaX);
			Log.i("ACTION_UP", "deltaX: " + deltaX + "; move:" + _move);// 图片事件处理完毕

			// 处理面板的显示逻辑事件
			
			Log.d("ACTION_UP.mTouchSlop", "mTouchSlop: " + deltaX);
			if (_move_real > 0) {// 右移
				slideOut();
			} else if (_move_real < 0) {// 左移
				slideIn();
			}
			resetViewScroll(_move_real);
			break;
		}
		return super.onTouchEvent(ev);
	}
	private void resetViewScroll(int deltaX) {
		_move = 0;
		scrollTo(0, 0);// TODO 改为平滑移动
	}

}
