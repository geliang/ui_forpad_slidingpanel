package com.slidingpad;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Scroller;
import android.widget.TextView;

public class SlidingStageLandscape extends SlidingStage {

	private int _slideWidthA;
	private int _marginLeftA;
	private int _slideWidthB;
	private int _marginLeftB;
	private float mLastMotionX;// 最后点击的点
	private int _move = 0;// 移动距离
	private static int MAXMOVE = 1300;// 最大允许的移动距离
	private static int _maxMoveToRight = 200;
	private int _rightExcessMove = 0;// 往右边多移的距离
	private int _leftExcessMove = 0;// 往左边多移的距离
	private final static int TOUCH_STATE_REST = 0;
	private final static int TOUCH_STATE_SCROLLING = 1;
	private int mTouchSlop;
	private int mTouchState = TOUCH_STATE_REST;
	private GestureDetector _gesture;
	private GestureDetector.SimpleOnGestureListener _gestureListener;
	private Scroller _scroller;
	private Animation _animDisappear;
	private Animation.AnimationListener _animListener;
	private Context _context;
	public SlidingStageLandscape(Context context, AttributeSet paramAttributeSet) {
		super(context, paramAttributeSet);
		this._context = context;
		this._slideWidthA = 660;
		this._marginLeftA = 306;
		this._slideWidthB = 660;
		this._marginLeftB = 660;
		this._gestureListener = simpleGListener;
		this._gesture = new GestureDetector(context, this._gestureListener);
		this._gesture.setIsLongpressEnabled(false);
		// this.setLongClickable(true);
		this._animListener = animListener;
		this._animDisappear = getDisappearAnim();
		final ViewConfiguration configuration = ViewConfiguration.get(context);
		// 获得可以认为是滚动的距离
		mTouchSlop = configuration.getScaledPagingTouchSlop();
		this._scroller = new Scroller(context, new OvershootInterpolator(1.5F));

		addView(this._slidingA);
		TextView tv = new TextView(context);
		tv.setText(Html.fromHtml("<br/><br/><br/> The Panel A<br/><br/>"));
		tv.setTextSize(30F);
		tv.setTextColor(android.graphics.Color.BLUE);
		this._slidingA.addView(tv, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		Button btn = new Button(context);
		btn.setText("Click Me to Show Panel B");
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showSlideB();
			}
		});
		this._slidingA.addView(btn, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ListView listview = new ListView(context);
		BaseAdapter baseadpater = new BaseAdapter() {
			
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				Button btn = new Button(_context);
				btn.setText("Click Me to Show Panel B");
				btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						showSlideB();
					}
				});
				return btn;
			}
			
			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return position;
			}
			
			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return 100;
			}
		};
		listview.setAdapter(baseadpater);
		this._slidingA.addView(listview, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		FrameLayout.LayoutParams layoutParamA = new FrameLayout.LayoutParams(this._slideWidthA,
				ViewGroup.LayoutParams.FILL_PARENT);
		// layoutParamA.setMargins(_marginLeftA, 0, 0, 0);
		this._slidingA.setLayoutParams(layoutParamA);
		// this._slidingA.offsetLeftAndRight(_marginLeftA);

		addView(this._slidingB, this._slideWidthB, LayoutParams.FILL_PARENT);
		FrameLayout.LayoutParams layoutParamB = new FrameLayout.LayoutParams(this._slideWidthB,
				ViewGroup.LayoutParams.FILL_PARENT);
		// layoutParamB.setMargins(_marginLeftB, 0, 0, 0);
		Button btn2 = new Button(context);
		btn2.setText("Click Me  ");
		TextView tvB = new TextView(context);
		tvB.setText(Html.fromHtml("<br/><br/><br/><br/> The Panel B<br/><br/>"));
		tvB.setTextSize(30F);
		tvB.setTextColor(android.graphics.Color.BLUE);
		this._slidingB.addView(btn2, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		this._slidingB.addView(tvB, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ListView listview2 = new ListView(_context);
		listview2.setAdapter(baseadpater);
		this._slidingB.addView(listview2, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		this._slidingB.setLayoutParams(layoutParamB);
		// this._slidingB.offsetLeftAndRight(_marginLeftB);

	}

	Animation.AnimationListener animListener = new AnimationListener() {

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub

		}
	};

	GestureDetector.SimpleOnGestureListener simpleGListener = new SimpleOnGestureListener() {
		public boolean onDown(MotionEvent paramMotionEvent) {
			return true;
		}

		public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float velocityX, float velocityY) {
			// if (velocityX > 200.0F) {
			// slideOut();
			// }
			// if (velocityX < -200.0F) {
			// slideIn();
			// }
			return false;
		}

		public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float velocityX, float velocityY) {

			int i = (int) (velocityX / 2.0F);
			// Toast.makeText(getContext(), "onScroll",
			// Toast.LENGTH_SHORT).show();
			Log.d("onScroll", "_move:" + _move);
			_slidingA.requestFocus();
			_slidingB.requestFocus();
			bringToFront();
			if (_leftExcessMove == 0 && _rightExcessMove == 0) {

				int slow = -(int) (velocityX / 2.0F);
				_scroller.fling(_move, 0, slow, 0, 0, MAXMOVE, 0, 0);
				_move = _scroller.getFinalX();
				computeScroll();
			}
			return false;
		}
	};

	public abstract interface onAdjustFinishListener {
		public abstract void a();
	}

	@Override
	public void slideIn() {
		if (this._slidingA.getVisibility() == View.VISIBLE) {
			return;
		}
		// invalidate();
		_marginLeftA = 306;
		AnimationSet animSet = new AnimationSet(true);
		animSet.setInterpolator(new OvershootInterpolator(1.5F));
		TranslateAnimation transAnim = new TranslateAnimation(this._slideWidthA, 0.0F, 0.0F, 0.0F);
		AlphaAnimation alphaAnim = new AlphaAnimation(0.0F, 1.0F);
		animSet.addAnimation(alphaAnim);
		animSet.addAnimation(transAnim);
		animSet.setDuration(400L);
		this._slidingA.startAnimation(animSet);
		this._slidingA.setVisibility(View.VISIBLE);
		requestLayout();
	}

	public void slideOut() {
		Log.d("slideOut",
				"_marginLeftA:" + _marginLeftA + ";" + this._slidingA.getVisibility() + " " + this._slidingB.getVisibility());

		if (this._slidingA.getVisibility() == View.VISIBLE && this._slidingB.getVisibility() == View.VISIBLE
				&& _marginLeftA == 62) {
			shrinkSlideB();
		} else if (this._slidingA.getVisibility() == View.VISIBLE && this._slidingB.getVisibility() == View.VISIBLE
				&& _marginLeftA == 306) {
			_marginLeftA = 306;
			this._slidingB.startAnimation(this._animDisappear);
			this._slidingB.setVisibility(View.INVISIBLE);
		} else if (this._slidingA.getVisibility() == View.VISIBLE
				&& (this._slidingB.getVisibility() == View.INVISIBLE || this._slidingB.getVisibility() == View.GONE)) {
			this._slidingA.setVisibility(View.INVISIBLE);
			this._slidingA.startAnimation(this._animDisappear);
		}
		Log.d("slideOut",
				"_marginLeftA:" + _marginLeftA + ";" + this._slidingA.getVisibility() + " " + this._slidingB.getVisibility());
		// invalidate();
		// requestLayout();
	}

	protected void shrinkSlideB() {
		if (this._slidingB.getVisibility() != View.VISIBLE) {
			return;
		}
		if (!this._scroller.isFinished()) {
			this._scroller.abortAnimation();
			this._scroller.forceFinished(true);
		}
		TranslateAnimation transAnim1 = new TranslateAnimation(-244.0F, 0.0F, 0.0F, 0.0F);
		TranslateAnimation transAnim2 = new TranslateAnimation(this._slideWidthB - 950, 0.0F, 0.0F, 0.0F);
		transAnim1.setDuration(270L);
		// transAnim1.setStartOffset(230L);
		transAnim2.setDuration(270L);

		Log.d("shrinkSlideB", "_marginLeftB:" + _marginLeftB);
		this._slidingA.startAnimation(transAnim1);
		this._slidingB.startAnimation(transAnim2);
		// this._slidingB.setVisibility(View.VISIBLE);
		_marginLeftA = 306;
		_marginLeftB = 950;
		requestLayout();
	}

	protected void showSlideB() {
		if (this._slidingB.getVisibility() == View.VISIBLE) {
			return;
		}
		TranslateAnimation transAnimA = new TranslateAnimation(244.0F, 0.0F, 0.0F, 0.0F);
		TranslateAnimation transAnimB = new TranslateAnimation(this._slideWidthB, 0.0F, 0.0F, 0.0F);
		transAnimA.setDuration(270L);
		transAnimA.setStartOffset(230L);
		transAnimB.setDuration(500L);
		this._slidingA.startAnimation(transAnimA);
		this._slidingB.startAnimation(transAnimB);
		this._slidingB.setVisibility(View.VISIBLE);
		_marginLeftA = 62;
		_marginLeftB = 660;
		requestLayout();
	}

	@Override
	public void welcome() {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if (_slidingA.getVisibility() == View.GONE || _slidingA.getVisibility() == View.INVISIBLE) {
					slideIn();
				}
			}
		};
		this._slidingA.postDelayed(runnable, 800L);
	}

//	protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
//		Log.d("onlayout", "_marginLeftA:" + _marginLeftA);
//		super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
//		this._slidingA.layout(_marginLeftA, this._slidingA.getTop(), (_marginLeftA + _slideWidthA), this._slidingA.getBottom());
//		this._slidingB.layout(_marginLeftB, this._slidingB.getTop(), (_marginLeftB + _slideWidthB), this._slidingB.getBottom());
//		// this.v.startScroll(0,0, 500, 500);
//	}

	private Animation getDisappearAnim() {
		AnimationSet animSet = new AnimationSet(true);
		animSet.setInterpolator(new LinearInterpolator());
		AlphaAnimation alphaAnim = new AlphaAnimation(1.0F, 0.0F);
		float f = this._slideWidthA / 3.0F;
		TranslateAnimation transAnim = new TranslateAnimation(0.0F, f, 0.0F, 0.0F);
		animSet.addAnimation(alphaAnim);
		animSet.addAnimation(transAnim);
		animSet.setDuration(200L);
		animSet.setAnimationListener(this._animListener);
		return animSet;
	}

	@Override
	public void computeScroll() {
		if (_scroller.computeScrollOffset()) {
			// 返回当前滚动X方向的偏移
			scrollTo(_scroller.getCurrX(), 0);
			postInvalidate();
		}
	}

	public boolean onInterceptTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		final float x = ev.getX();
		Log.d("SlidingStageLandscape.InterceptTouchEvent", "ev.getX():" + x + "; ev.getAction():" + ev.getAction());
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mLastMotionX = x;
			mTouchState = _scroller.isFinished() ? TOUCH_STATE_REST : TOUCH_STATE_SCROLLING;
			mTouchState = TOUCH_STATE_REST;

			break;
		case MotionEvent.ACTION_MOVE:
			final int xDiff = (int) Math.abs(x - mLastMotionX);
			// 判断是否是移动
			Log.d("SlidingStageLandscape.InterceptTouchEvent", "xDiff:" + xDiff + "; mTouchSlop:" + mTouchSlop);
			if (xDiff > mTouchSlop) {
				mTouchState = TOUCH_STATE_SCROLLING;
				Log.d("SlidingStageLandscape.InterceptTouchEvent", "xDiff > mTouchSlop,it`s planel sroll event,return true to InterceptTouchEvent");
				return true;
			}
		case MotionEvent.ACTION_UP:
			mTouchState = TOUCH_STATE_REST;
			break;
		}
		return false;
		// return mTouchState != TOUCH_STATE_REST;
	}

	public boolean onTouchEvent(MotionEvent ev) {
		// final int action = ev.getAction();

		Log.d("SlidingStageLandscape.onTouchEvent",
				"ev.getAction():" + ev.getAction() + "; super.onTouchEvent(ev):" + super.onTouchEvent(ev)
						+ ";;this._gesture.onTouchEvent(ev):" + this._gesture.onTouchEvent(ev));
		int i = ev.getAction();
		
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if(ev.getX()<_marginLeftA)
				return false;
			break;}
		
		o(ev);

		super.onTouchEvent(ev);
		return this._gesture.onTouchEvent(ev);
		// return false;
		// return this._gesture.onTouchEvent(ev);
		// this._gesture.onTouchEvent(ev);
		// return super.onTouchEvent(ev);
	}

	private void o(MotionEvent ev) {
		final float x = ev.getX();
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (!_scroller.isFinished()) {
				_scroller.forceFinished(true);
				_move = _scroller.getFinalX();
			}
			mLastMotionX = x;
			break;
		case MotionEvent.ACTION_MOVE:
			if (ev.getPointerCount() == 1) {
				g();
				int deltaX = 0;
				deltaX = (int) (mLastMotionX - x);
				mLastMotionX = x;
				Log.d("Moving", "deltaX: " + deltaX + "; move:" + _move);
				if (deltaX < 0) {
					// move to right
					if (_rightExcessMove == 0) {
						if (_move > 0) {
							int move_this = Math.max(-_move, deltaX);
							_move = _move + move_this;
							scrollBy(move_this, 0);
							Log.d("_leftExcessMove 364", "_leftExcessMove:" + _leftExcessMove + "; move_this:" + move_this);
						} else if (_move == 0) { // 如果已经是最右端 继续往右拉
							_leftExcessMove = _leftExcessMove - deltaX / 2;// 记录下多往右拉的值
							scrollBy(deltaX / 2, 0);
							Log.d("_leftExcessMove 368", "_leftExcessMove:" + _leftExcessMove + ";_move:" + _move);
						}
					} else if (_rightExcessMove > 0)// 之前有右移过头
					{
						Log.d("up_excess_move>0", "up_excess_move:" + _rightExcessMove + "; deltaY:" + deltaX);
						if (_rightExcessMove >= (-deltaX)) {
							_rightExcessMove = _rightExcessMove + deltaX;
							scrollBy(deltaX, 0);
						} else {
							_rightExcessMove = 0;
							scrollBy(-_rightExcessMove, 0);
						}
					}
				} else if (deltaX > 0) {
					// move to left
					//
					Log.d("Moving", "deltaX: " + deltaX + "; move:" + _move + "; _leftExcessMove:" + _leftExcessMove);
					if (_leftExcessMove == 0) {
						// if (MAXMOVE - _move > 0) {
						if (_move > 0) {
							// int move_this = Math.min(MAXMOVE - _move,
							// deltaX);
							int move_this = Math.min(_move, deltaX);
							_move = _move + move_this;
							scrollBy(move_this, 0);
							Log.d("Moving 395", "deltaX: " + deltaX + "; move:" + _move + "; _leftExcessMove:" + _leftExcessMove
									+ ";move_this:" + move_this);
						} else if (_move == 0) {
							// } else if (MAXMOVE - _move == 0) {
							// if (_rightExcessMove <= 100) {
							_rightExcessMove = _rightExcessMove + deltaX / 2;
							scrollBy(deltaX / 2, 0);
							// }
							Log.d("Moving 404", "deltaX: " + deltaX + "; move:" + _move + "; _leftExcessMove:" + _leftExcessMove);
						}
					} else if (_leftExcessMove > 0) {
						if (_leftExcessMove >= deltaX) {
							_leftExcessMove = _leftExcessMove - deltaX;
							scrollBy(deltaX, 0);
						} else {
							_leftExcessMove = 0;
							scrollBy(_leftExcessMove, 0);
						}
					}

					Log.d("Moving", "deltaX: " + deltaX + "; move:" + _move + "; _leftExcessMove:" + _leftExcessMove);
				}
			}
			break;
		case MotionEvent.ACTION_UP:

			Log.d("Move Up", "_rightExcessMove: " + _rightExcessMove + "; _leftExcessMove:" + _leftExcessMove);
			// 多滚是负数 记录到move里
			if (_rightExcessMove > 0) {
				// 多滚了 要弹回去
				this._scroller.startScroll(_rightExcessMove, 0, -_rightExcessMove, 0, 500);
				// scrollBy(-_rightExcessMove, 0);
				invalidate();
				_rightExcessMove = 0;
			}
			if (_leftExcessMove > 0) {
				// 多滚了 要弹回去
				// this._scroller.scrollBy(_leftExcessMove, 0);
				if (_leftExcessMove > _maxMoveToRight) {
					// this._scroller.startScroll(_maxMoveToRight
					// - _leftExcessMove, 0, _leftExcessMove
					// - _maxMoveToRight, 0, 500);
					slideOut();
				} else {
				}
				this._scroller.startScroll(-_leftExcessMove, 0, _leftExcessMove, 0, 500);
				invalidate();
				_leftExcessMove = 0;
			}
			mTouchState = TOUCH_STATE_REST;
			Log.d("Move Up", "_rightExcessMove: " + _rightExcessMove + "; _leftExcessMove:" + _leftExcessMove);
			break;
		}
	}
	// @Override
	// public boolean dispatchTouchEvent(MotionEvent event) {
	//
	// if (this._gesture.onTouchEvent(event)) {
	// event.setAction(MotionEvent.ACTION_CANCEL);
	// }
	// return super.dispatchTouchEvent(event);
	// }
}
